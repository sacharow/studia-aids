class Node:
    def __init__(self,x):
        self.key = x
        self.left = None # lewy syn
        self.right = None # prawy syn
        self.p = None # ojciec

class BST:
    def __init__(self):
        self.root = None

    def BSTsearch(self,k):
        # szuka wezla zawierajacego klucz k
        x = self.root
        while x!=None and x.key!=k:
            if k<x.key:
                x=x.left
            else:
                x=x.right
        return x # None oznacza, ze szukanego klucza
        # nie ma w drzewie 
    def BSTinsert(self, z):
        # wstawia wezel z do drzewa
        z=Node(z)
        x = self.root
        y = None # y jest ojcem x
        while x != None:
            y=x
            if z.key<x.key:
               x=x.left
            else:
               x=x.right
        z.p=y
        if y==None: # drzewo puste
            self.root=z
        else:
            if z.key<y.key:
                y.left=z
            else:
                y.right=z 


element=BST()
node1=element.BSTinsert(1)