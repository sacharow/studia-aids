import os
import random

# ZAMIANA TEKSTU NA WARTOŚĆ INT
def liczba(k):
    result = 0
    for i in range(len(k)):
        if i == len(k) - 1:
            result = result + 111
        elif i % 2 == 0:
            result = result + (111 * ord(k[i]))
        else:
            result = result + (111 + ord(k[i]))
    return result

# SZUKANIE ELEMENTU
def search(m, tab, key, word):
    for i in range(m):
        if i == m - 1:
            print("Nie znaleziono klucza:", key)
            return 0
        else:
            hashedValue = (liczba(word) + i) % m
            if tab[hashedValue] == key:
                print(key, "znaleziono w indeksie:", hashedValue, ", 'i' wynosi:", i)
                return 0

# SPRAWDZANIE CZY MIEJSCE JEST PUSTE I WRZUCENIE KLUCZA DO TABLICY
def insert(m, tab, value, key):
    shortshort = 0
    for i in range(m):
        if tab[i] == None or tab[i]=='DEL':
            shortshort += 1
    if shortshort == 0:
        return tab, 0

    for i in range(m):
        hashedValue = (value + i) % m
        if tab[hashedValue] == None or tab[hashedValue]=='DEL':
            tab[hashedValue] = key
            return tab, i

# OPERACJA USUWANIA
def delete(m, tab, key):
    for i in range(m):
        if tab[i] == key:
            tab[i] = 'DEL'

# Funkcja usuwająca losową połowę elementów z tablicy
def remove_random_half(m, tab):
    idToDelete = random.sample(range(m), m // 2)  # Losujemy połowę indeksów
    for idx in idToDelete:
        if tab[idx] is not None:
            delete(m, tab, tab[idx])  # Usuwamy element na wylosowanym indeksie

# OGARNIANIE DANYCH Z PLIKU
with open("nazwiska.txt", 'r') as file:
    datalines = file.readlines()
lines_without_newlines = [line.strip() for line in datalines]
text = [line.split(" ") for line in lines_without_newlines]
keys = []
objects = []
for line in text:
    obj = {line[0], line[1]}
    objects.append(obj)
    keys.append(line[1])
keyAmount = len(text)

# TWORZENIE PUSTEJ TABLICY O WIELKOŚCI M - PEŁNA
m = 89
tab = [None] * m

# TWORZENIE I WKŁADANIE KLUCZY DO TABLICY - PEŁNA
licznik = 0
for i in range(keyAmount):
    value = liczba(keys[i])
    tab, suma = insert(m, tab, value, objects[i])
    licznik = licznik + suma

# LICZENIE SREDNIEJ - PEŁNA
print("zapełnienie 100%")
if keyAmount > m:
    print("srednia ilosc 'i':", licznik / m)
else:
    print("srednia ilosc 'i':", licznik / (m - keyAmount))

# OPERACJA SZUKAJ
search(m, tab, {'17278', 'Ratajczak'}, 'Ratajczak')
search(m, tab, {'5399', 'Szopa'}, 'Szopa')
search(m, tab, {'779', 'Poreda'}, 'Poreda')
search(m, tab, {'779', 'Laskus'}, 'Laskus')
search(m, tab, {'5711', 'Kraszewski'}, 'Kraszewski')
search(m, tab, {'5708', 'Makuch'}, 'Makuch')
search(m, tab, {'5708', 'Kosmala'}, 'Kosmala')
search(m, tab, {'5679', 'Kozik'}, 'Kozik')

# WYPIS TABLICY - PEŁNA
# for i in range(len(tab)):
#     print(tab[i])

# OPERACJA W - 50%/70%/90%
tab = [None] * m
licznik = 0
for i in range(keyAmount - 100):
    value = liczba(keys[i])
    tab, suma = insert(m, tab, value, objects[i])
    licznik = licznik + suma
print("zapełnienie 50%")
if keyAmount > m:
    print("srednia ilosc 'i':", licznik / m)
else:
    print("srednia ilosc 'i':", licznik / (m - keyAmount))

# OPERACJA W - 70%
tab = [None] * m
licznik = 0
for i in range(keyAmount - 82):
    value = liczba(keys[i])
    tab, suma = insert(m, tab, value, objects[i])
    licznik = licznik + suma
print("zapełnienie 70%")
if keyAmount > m:
    print("srednia ilosc 'i':", licznik / m)
else:
    print("srednia ilosc 'i':", licznik / (m - keyAmount))

# OPERACJA W - 90%
tab = [None] * m
licznik = 0
for i in range(keyAmount - 64):
    value = liczba(keys[i])
    tab, suma = insert(m, tab, value, objects[i])
    licznik = licznik + suma
print("zapełnienie 90%")
if keyAmount > m:
    print("srednia ilosc 'i':", licznik / m)
else:
    print("srednia ilosc 'i':", licznik / (m - keyAmount))

# OPERACJA USUWANIA LOSOWEJ POŁOWY ELEMENTÓW
remove_random_half(m, tab)
licznik = 0
for i in range(keyAmount - 100):
    value = liczba(keys[i])
    tab, suma = insert(m, tab, value, objects[i])
    licznik = licznik + suma
# ZLICZANIE WYPEŁNIONYCH POZYCJI ZNACZNIKIEM DEL
deleted_count = sum(1 for item in tab if item == 'DEL')
print("Liczba pozycji w tablicy wypełnionych znacznikiem DEL:", deleted_count)