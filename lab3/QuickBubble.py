from timeit import default_timer as timer
import random

def anothersort(A,p,r,zmienna):
    #print("Bąbelek Start",A)
    for i in range(p,r):
        for j in range(p+1,r+1):
            if A[i]>A[j]:
                A[i],A[j]=A[j],A[i]
    #print(A,"Bąbelek Koniec")
    return A

def quickSort(A,p,r,zmienna):
    if p<r :
        #print("długość:",r-p+1)
        if(r-p+1<=zmienna):
            anothersort(A,p,r,zmienna)
        q = partition(A,p,r,zmienna)
        quickSort(A,p,q,zmienna)
        quickSort(A,q+1,r,zmienna)
def partition(A,p,r,zmienna):
    x=A[r] # element wyznaczajacy podział
    #print("Piwot=",x)
    i=p-1
    for j in range (p, r+1):
        if A[j]<=x :
            i=i+1
            A[i], A[j] = A[j], A[i]
            #print(A)
    #print(A)
    if i<r :
        return i
    else:
        return i-1
    
A=[]
n=random.randint(1,20)
for i in range(n):
    x=random.randint(1,100)
    A.append(x)

zmienna=3
#print(A)
start = timer()
quickSort(A,0,len(A)-1,zmienna)
stop = timer()
Tn=stop-start
Fn=len(A)
print(len(A), Tn, Fn/Tn)
#print(A)

A=[1,2,3,4,5,6,7,8,9,10,11,12,13]
zmienna=3
#print(A)
start = timer()
quickSort(A,0,len(A)-1,zmienna)
stop = timer()
Tn=stop-start
Fn=len(A)
print(len(A), Tn, Fn/Tn)
#print(A)

A=[23,6,11,12,17,19,7,18,12,14,15,3,2] 
zmienna=3
#print(A)
start = timer()
quickSort(A,0,len(A)-1,zmienna)
stop = timer()
Tn=stop-start
Fn=len(A)
print(len(A), Tn, Fn/Tn)
#print(A)
