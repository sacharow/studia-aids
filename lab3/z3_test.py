from timeit import default_timer as timer

def quickSort(A,p,r):
    if p<r :
        q = partition(A,p,r)
        quickSort(A,p,q)
        quickSort(A,q+1,r)

def partition(A,p,r):
    x=A[r] # element wyznaczajacy podział
    i=p-1
    for j in range (p, r+1):
        if A[j]<=x :
            i=i+1
            A[i], A[j] = A[j], A[i]
    if i<r :
        return i
    else:
        return i-1

A=[23,6,11,12,17,19,7,18,12,14,15]
print(A)

start = timer()
quickSort(A,0,len(A)-1)
stop = timer()
Tn=stop-start
Fn=len(A)
print(len(A), Tn, Fn/Tn)

print(A)



