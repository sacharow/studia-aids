import os

def checker(lista,m):
    zero=0
    maxi=0
    srednia=0
    srednia2=0
    for i in range(m):
        if len(lista[i])==0:
            zero+=1
        else:
            if len(lista[i])>maxi:
                maxi=len(lista[i])
        if len(lista[i])>0:
            srednia=srednia+len(lista[i])
    srednia=srednia/(m-zero)
    print("puste tablice:",zero," maksymalna dlugosc:",maxi," srednia dlugosc:",srednia)

def liczba(k):
    result=0
    for i in range(len(k)):
        if i==len(k)-1:
            result=result+111
        elif i%2==0:
            result=result+(111*ord(k[i]))
        else:
            result=result+(111+ord(k[i]))
    return result

def liczbaBad(k):
    result=0
    for i in range(len(k)):
        result=result+(ord(k[i]))
    return result

with open("keys.txt", 'r') as file:
    datalines=file.readlines()

lines_without_newlines = [line.strip() for line in datalines]
keys=[]
for line in lines_without_newlines:
    keys.append(line)

keyAmount=len(datalines)
newKeyAmount=keyAmount
m=1024

##################################################################################### HASH
T={}
for i in range(0,m):
    T[i]=[]

for i in range(0,newKeyAmount):
    h=hash(keys[i])%m
    T[h].append(keys[i])

with open("answer1.txt", 'w') as file:
    file.writelines(str(T))

checker(T,m)
##################################################################################### SCHEMAT KONWERSJI 1
T={}
for i in range(0,m):
    T[i]=[]

for i in range(0,newKeyAmount):
    h=liczba(keys[i])%m
    T[h].append(keys[i])

with open("answer2.txt", 'w') as file:
    file.writelines(str(T))

checker(T,m)
##################################################################################### SCHEMAT KONWERSJI 2 - BAD
T={}
for i in range(0,m):
    T[i]=[]

for i in range(0,newKeyAmount):
    h=liczbaBad(keys[i])%m
    T[h].append(keys[i])

with open("answer3.txt", 'w') as file:
    file.writelines(str(T))

checker(T,m)