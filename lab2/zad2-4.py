def buildHeap(A):
    heapSize=len(A)
    k=int((len(A)-2)/2)
    for i in range(k,-1,-1):
        heapify(A,heapSize,i)
    return A
def heapify(A, heapSize, i):
    while True:
        l = 2 * i + 1  # lewy syn
        r = 2 * i + 2  # prawy syn
        largest = i
        if l < heapSize and A[l] > A[largest]:
            largest = l
        if r < heapSize and A[r] > A[largest]:
            largest = r
        if largest != i:
            A[i], A[largest] = A[largest], A[i]
            i = largest
        else:
            break
    return A
def heapSort(A):
    A = buildHeap(A)
    heapSize = len(A)
    for i in range(len(A)-1, 0, -1):
        A[0], A[heapSize-1] = A[heapSize-1], A[0]
        heapSize -= 1
        heapify(A,heapSize,0)
    return A

A=[28, 6, 11, 12, 17, 8, 7, 18, 12, 14, 23]
##print(buildHeap(A))
print(heapSort(A))

