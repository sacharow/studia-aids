from timeit import default_timer as timer

class Node:
    def __init__(self,k):
        self.key=k
        self.prev= None
        self.next= None

class DoublelinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def wstaw(self,x):
        x.next = self.head
        if self.head is not None:
            self.head.prev =x
        self.head =x
        x.prev = None
    
    def drukuj(self):
        x = self.head
        while x is not None:
            print(x.key)
            x=x.next

    def szukaj(self,y):
        x= self.head
        while x is not None:
            if y==x.key:
                return x.key
            else:
                x=x.next
        return None

    def usun(self,y):
        x= self.head
        while x is not None:
            #drukuj(".",x.key)
            if y==x.key:
                if x.prev is not None:
                    x.prev.next=x.next
                else:
                    self.head=x.next
                if x.next is not None:
                    x.next.prev = x.prev
                x.key=None
            x=x.next
        return self

    def nowaLista(self):
        unique_words = set()
        result = DoublelinkedList()
        current = self.head
        while current:
            if current.key not in unique_words:
                result.wstaw(Node(current.key))
                unique_words.add(current.key)
            current = current.next
        return result
    
def scal(lista,lista2):
        x=lista.head
        y=lista2.head
        result=DoublelinkedList()
        while x is not None:
            result.wstaw(Node(x.key))
            x=x.next
        while y is not None:
            result.wstaw(Node(y.key))
            y=y.next
        return result

def scal2(lista,lista2):
        x=lista.tail
        y=lista2.head
        result=lista
        while y is not None:
            result.wstaw(Node(y.key))
            y=y.next

        return result

lista=DoublelinkedList()

print("---wypisz---")
lista.drukuj()

print("---dodaj słowa---")
lista.wstaw(Node("aaa"))
lista.wstaw(Node("ba"))
lista.wstaw(Node("ab"))
lista.wstaw(Node("c"))
lista.wstaw(Node("a"))
lista.wstaw(Node("b"))
lista.wstaw(Node("a"))
lista.wstaw(Node("oc"))
lista.wstaw(Node("ab"))
lista.wstaw(Node("b"))
lista.wstaw(Node("a"))
lista.wstaw(Node("c"))
lista.wstaw(Node("a"))
lista.wstaw(Node("ba"))
lista.wstaw(Node("a"))
lista.wstaw(Node("c"))
lista.wstaw(Node("av"))
lista.wstaw(Node("bcc"))
lista.wstaw(Node("a"))
lista.wstaw(Node("c"))
lista.wstaw(Node("av"))
lista.wstaw(Node("b"))
lista.wstaw(Node("a"))
lista.wstaw(Node("ac"))
lista.wstaw(Node("ac"))
lista.wstaw(Node("b"))
lista.wstaw(Node("aa"))
lista.wstaw(Node("c"))
lista.wstaw(Node("a"))
lista.wstaw(Node("b"))
lista.wstaw(Node("a"))
lista.wstaw(Node("ca"))

print("---wypisz---")
lista.drukuj()

print("---szukaj słowa---")
print(lista.szukaj("a"))

print("---usuń słowo---")
lista.usun("aa")
lista.drukuj()

print("---nowa lista--")
lista2=lista.nowaLista()
lista2.drukuj()

##################################################################
n=42
print("---dodawanie list--")
start = timer()
lista3=scal(lista,lista2)
#lista3.drukuj()

stop = timer()
Tn=stop-start
Fn=n
print(n, Tn, Fn/Tn)
##################################################################
n=42
print("---scalanie list (dodawanie v2)---")
start = timer()
lista4=scal2(lista,lista2)
#lista4.drukuj()

stop = timer()
Tn=stop-start
Fn=n
print(n, Tn, Fn/Tn)
##################################################################